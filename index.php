<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php
    require_once('animal.php');
    require_once('ape.php');
    require_once('frog.php');

    $sheep = new Animal("Shaun");

    echo "Name : " . $sheep->name . "<br>";
    echo "Legs : " . $sheep->legs . "<br>";
    echo "Cold blooded : " . $sheep->cold_blooded . "<br>";

    echo "<br><br>";

    $sungokong = new Ape("Kera Sakti");
    echo "Name : " . $sungokong->name . "<br>";
    echo "Legs : " . $sungokong->legs . "<br>";
    echo "Cold blooded : " . $sungokong->cold_blooded . "<br>";
    echo "Yell : " . $sungokong->yell();

    echo "<br><br>";

    $kodok = new Frog("buduk");
    echo "Name : " . $kodok->name . "<br>";
    echo "Legs : " . $kodok->legs . "<br>";
    echo "Cold blooded : " . $kodok->cold_blooded . "<br>";
    echo "Jump : " . $kodok->jump();
    ?>
</body>

</html>